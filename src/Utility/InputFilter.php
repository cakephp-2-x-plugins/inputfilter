<?php
namespace InputFilter\Utility;

use Cake\I18n\Time;

/**
 * A library for convenience wrappers to validation and sanitization functions.
 * All functions that validate include a $validateOnly option (Default: false)
 * as their last parameter. Note: Some filters will only exists as validate or
 * sanitize.
 *
 * @author Jason Burgess <jason@notplugged.in>
 * @version 3.0
 */
class InputFilter
{

    /**
     *
     * @var array List of sanitize filters
     */
    protected static $sanitizeFilters = array(
        'email' => FILTER_SANITIZE_EMAIL,
        'encodeHtml' => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
        'float' => FILTER_VALIDATE_FLOAT,
        'int' => FILTER_SANITIZE_NUMBER_INT,
        'stripHtml' => FILTER_SANITIZE_STRING,
        'url' => FILTER_SANITIZE_URL
    );

    /**
     *
     * @var array List of validate filters
     */
    protected static $validateFilters = array(
        'email' => FILTER_VALIDATE_EMAIL,
        'float' => FILTER_VALIDATE_FLOAT,
        'int' => FILTER_VALIDATE_INT,
        'url' => FILTER_VALIDATE_URL
    );

    /**
     *
     * @var array List of options for sanitize filters
     */
    protected static $sanitizeOptions = array(
        'stripHtml' => array(
            FILTER_FLAG_ENCODE_LOW,
            FILTER_FLAG_ENCODE_HIGH,
            FILTER_FLAG_ENCODE_AMP
        )
    );

    /**
     *
     * @var array List of options for validate filters
     */
    protected static $validateOptions = array();

    /**
     *
     * @var array List of regular expression for validation
     */
    protected static $validateRegEx = array(
        'slugUpper' => '/^[a-z0-9_]+$/i',
        'slug' => '/^[a-z0-9_]+$/',
        'slugSpace' => '/^[a-z0-9 ]+$/',
        'url' => '/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i',
        'uuid' => '/^[a-z0-9-]{36}$/i'
    );

    /**
     *
     * @var array List of regular expression for sanitization
     */
    protected static $sanitizeRegEx = array(
        'slug' => '/[^a-z0-9_]+/i',
        'slugUpper' => '/[^a-z0-9_]+/i',
        'slugSpace' => '/[^a-z0-0 ]+/i',
        'uuid' => '/[^a-z0-9-]+/i'
    );

    /**
     * Sanitize a date
     *
     * @param mixed $input
     *            Date to check
     * @param string $inputFormat
     *            Specific format to check, null to allow any. (Default:
     *            'yyyy-MM-dd')
     * @param bool $validateOnly
     *            Validate instead of sanitize (Default: false)
     * @return Cake\I18n\Time bool date, or validation result.
     */
    public static function date($input, $inputFormat = 'Y-m-d', $validateOnly = false)
    {
        if (is_string($input)) {
            $date = Time::createFromFormat($inputFormat, $input);
        } else {
            // Assume it is a \Cake\I18n\Time object (or at least \DateTime)
            $date = $input;
        }

        if (!$date) {
            // ALWAYS ensure it's a valid date.
            return false;
        }

        if ($validateOnly) {
            return true;
        }

        return $date;
    }

    /**
     * Filter an email address
     *
     * @param string $input
     *            Email address to check
     * @param bool $validateOnly
     *            Validate instead of sanitize (Default: false)
     * @return string bool filtered email address, or the validity.
     * @since 1.0
     * @access public
     */
    public static function email($input, $validateOnly = false)
    {
        return self::runFilter($input, 'email', $validateOnly);
    }

    /**
     * Filter a float.
     *
     * @param float $input
     * @param bool $validateOnly
     *            Validate instead of sanitize (Default: false)
     * @return float bool filtered float, or the validity.
     */
    public static function float($input, $validateOnly = false)
    {
        return self::runFilter($input, 'float', $validateOnly);
    }

    /**
     * Filter an integer
     *
     * @param int $input
     * @param bool $validateOnly
     *            Validate instead of sanitize (Default: false)
     * @return int bool filtered integer, or the validity.
     */
    public static function int($input, $validateOnly = false)
    {
        return self::runFilter($input, 'int', $validateOnly);
    }

    /**
     * Filter a string to remove all but alphanumeric characters.
     * Also allow for
     * including other characters.
     *
     * @param string $input
     * @param bool $validateOnly
     *            Validate instead of sanitize (Default: false)
     * @return string bool filtered string, or the validity.
     */
    public static function paranoid($input, $validateOnly = false, $extraChars = '')
    {
        // Rebuild the filters every time.
        $filter = sprintf('/^[a-z0-9%s]+$/i', $extraChars);
        $sanitize = sprintf('/[^a-z0-9%s]/i', $extraChars);
        
        self::$validateRegEx['paranoid'] = $filter;
        self::$sanitizeRegEx['paranoid'] = $sanitize;

        return trim(self::runRegularExpression($input, 'paranoid', $validateOnly));
    }

    /**
     * Slug a text string, always lowercase.
     *
     * @param string $input
     * @param bool $validateOnly
     *            Validate instead of sanitize (Default: false)
     * @return string bool slugged text, or the validity.
     * @see InputFilter::slugUpper()
     */
    public static function slug($input, $validateOnly = false)
    {
        $output = self::runRegularExpression($input, 'slug', $validateOnly);

        // Always ensure slugs are lowercase
        if (is_string($output)) {
            return strtolower($output);
        }

        return $output;
    }

    /**
     * Slug a text string, with spaces allowed.
     *
     * @param string $input
     * @param bool $validateOnly
     *            Validate instead of sanitize (Default: false)
     * @return string bool slugged text, or the validity
     * @see InputFilter::slug()
     */
    public static function slugSpace($input, $validateOnly = false)
    {
        // This one allows uppercase.
        return self::runRegularExpression($input, 'slugSpace', $validateOnly);
    }

    /**
     * Slug a text string, with uppercase characters allowed.
     *
     * @param string $input
     * @param bool $validateOnly
     *            Validate instead of sanitize (Default: false)
     * @return string bool slugged text, or the validity
     * @see InputFilter::slug()
     */
    public static function slugUpper($input, $validateOnly = false)
    {
        // This one allows uppercase.
        return self::runRegularExpression($input, 'slugUpper', $validateOnly);
    }

    /**
     * Strip all html from a string.
     *
     * @param string $input
     * @return string Filtered text.
     */
    public static function stripHtml($input)
    {
        return trim(self::runFilter($input, 'stripHtml'));
    }

    /**
     * Filter input to encode html entities.
     *
     * @param string $input
     * @return string Filtered text.
     */
    public static function encodeHtml($input)
    {
        return trim(self::runFilter($input, 'encodeHtml'));
    }

    /**
     * Filter a url
     *
     * @param string $input
     * @param bool $validateOnly
     *            Validate instead of sanitize (Default: false)
     * @return string bool filtered url, or the validity
     */
    public static function url($input, $validateOnly = false)
    {
        return self::runFilter($input, 'url', $validateOnly);
    }

    /**
     * Filter a uuid (or guid if you prefer).
     *
     * @param string $input
     * @param bool $validateOnly
     *            Validate instead of sanitize (Default: false)
     * @return string bool uuid, or the validity
     */
    public static function uuid($input, $validateOnly = false)
    {
        // @todo add support for checking/fixing missing hypens
        return self::runRegularExpression($input, 'uuid', $validateOnly);
    }

    /**
     * Run a defined filter.
     *
     * @param mixed $input
     *            Input text.
     * @param string $filterName
     *            Name of the filter to run.
     * @param bool $validateOnly
     *            Validate instead of sanitize (Default: false)
     * @return mixed bool filtered result, or the validity
     */
    protected static function runFilter($input, $filterName, $validateOnly = false)
    {
        $filter = false;
        $options = self::getOptions($filterName, $validateOnly);

        // Get requested filter and options
        if ($validateOnly) {
            // Validation
            if (isset(self::$validateFilters[$filterName])) {
                $filter = self::$validateFilters[$filterName];
            }
        } else {
            // Sanitization
            if (isset(self::$sanitizeFilters[$filterName])) {
                $filter = self::$sanitizeFilters[$filterName];
            }
        }

        if (! $filter) {
            // No filter was found, so we fail quietly.
            return false;
        }

        return filter_var($input, $filter, $options);
    }

    /**
     * Run a regular expression filter.
     *
     * @param string $input
     * @param string $filterName
     *            Name of the filter to run.
     * @param bool $validateOnly
     *            Validate instead of sanitize (Default: false)
     * @return string bool filtered result, or the validity.
     */
    protected static function runRegularExpression($input, $filterName, $validateOnly = false)
    {
        $regex = false;
        $output = false;

        if ($validateOnly) {
            if (isset(self::$validateRegEx[$filterName])) {
                $regex = self::$validateRegEx[$filterName];
            }
        } else {
            if (isset(self::$sanitizeRegEx[$filterName])) {
                $regex = self::$sanitizeRegEx[$filterName];
            }
        }

        if ($regex == false) {
            // No filter was found, so we fail quietly.
            return false;
        }

        if ($validateOnly) {
            $output = preg_match($regex, $input);
        } else {
            $output = preg_replace($regex, '', $input);
        }

        return $output;
    }

    /**
     * Get the options for the requested filter.
     *
     * @param string $filterName
     * @param bool $validateOnly
     *            Validate instead of sanitize (Default: false)
     * @return int Flags for filter
     */
    protected static function getOptions($filterName, $validateOnly = false)
    {
        $options = 0;

        // Get the right options for the filter.
        if ($validateOnly) {
            if (isset(self::$validateOptions[$filterName])) {
                $options = self::$validateOptions[$filterName];
            }
        } else {
            if (isset(self::$sanitizeOptions[$filterName])) {
                $options = self::$sanitizeOptions[$filterName];
            }
        }

        // Because we can't bitwise or them in the declaration
        if (is_array($options)) {
            $newOptions = 0;
            foreach ($options as $option) {
                $newOptions |= $option;
            }
            return $newOptions;
        }

        return $options;
    }
}