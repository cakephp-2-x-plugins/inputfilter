<?php
namespace InputFilter\Utility;

use Latte\Engine;
use Latte\Loaders\StringLoader;

class Tokenizer
{

    protected static $latte = false;

    public static function replace($format, array $data)
    {
        $latte = self::getInstance();

        return $latte->renderToString($format, $data);
    }

    protected static function getInstance()
    {
        if (self::$latte == false) {
            self::$latte = new Engine();
            self::$latte->setTempDirectory(TMP . DS . 'latte');
            self::$latte->setLoader(new StringLoader());

            // Add filters for slugs
            self::$latte->addFilter('slug', function ($s)
            {
                return InputFilter::slug($s);
            });
            self::$latte->addFilter('slugUpper', function ($s)
            {
                return InputFilter::slugUpper($s);
            });

            // Add a zero padded number filter
            self::$latte->addFilter('zeroNumber', function ($s, $len = 1) {
            	return sprintf('%0' . $len . 'd', $s);
            });
        }

        return self::$latte;
    }
}